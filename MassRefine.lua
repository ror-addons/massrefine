
MassRefine={}
MassRefine.sWindowName="MassRefine"

-- Current time
MassRefine.nTime=0

-- How fast is a double click?
MassRefine.nDoubleClickTime=.5  --Seconds

-- Keep track of the last thing clicked and when
MassRefine.nWaitingSlot=0
MassRefine.nWaitingLoc=0
MassRefine.nWaitingSince=0

-- Holds itemData of item dialog is processing
--MassRefine.vDialogItem

-- OnInitialize Handler
function MassRefine.Initialize()		
  CreateWindow(MassRefine.sWindowName,false)

  if EA_Window_Backpack.POCKET_MAIN_INVENTORY_INDEX then
    MassRefine.TYPE_INVENTORY=EA_Window_Backpack.POCKET_MAIN_INVENTORY_INDEX
  else
    MassRefine.TYPE_INVENTORY=EA_Window_Backpack.TYPE_INVENTORY
  end
  MassRefine.TYPE_CRAFTING=EA_Window_Backpack.TYPE_CRAFTING

  LabelSetText(MassRefine.sWindowName.."TitleBarText",L"Mass Refine")
  ButtonSetText(MassRefine.sWindowName.."Okay",GetString(StringTables.Default.LABEL_OKAY))
  ButtonSetText(MassRefine.sWindowName.."Cancel",GetString(StringTables.Default.LABEL_CANCEL))
  WindowClearAnchors(MassRefine.sWindowName.."TitleBarText")
  WindowAddAnchor(MassRefine.sWindowName.."TitleBarText","topleft",MassRefine.sWindowName.."TitleBar","topleft",0,5)  
  WindowAddAnchor(MassRefine.sWindowName.."TitleBarText","topright",MassRefine.sWindowName.."TitleBar","topright",0,5)  

  MassRefine.OldConfirmThenRefine=EA_Window_Backpack.ConfirmThenRefine
  EA_Window_Backpack.ConfirmThenRefine=MassRefine.NewConfirmThenRefine
end

-- OnUpdate Handler
function MassRefine.OnUpdate(nTimeElapsed)
  MassRefine.nTime=MassRefine.nTime+nTimeElapsed

  -- If we're waiting for a second click and it didn't come, do the single click behavior
  if (MassRefine.nWaitingSlot~=0)and((MassRefine.nTime-MassRefine.nWaitingSince)>MassRefine.nDoubleClickTime) then
    local nSlot=MassRefine.nWaitingSlot
    local nLoc=MassRefine.nWaitingLoc
    MassRefine.nWaitingSlot=0 --This way an error in external code doesn't cause unintended behaviour
    MassRefine.nWaitingLoc=0
    MassRefine.OldConfirmThenRefine(nSlot,nLoc)
  end
end


-- This function replaces the default refining handler
function MassRefine.NewConfirmThenRefine(nClickSlot,nClickLoc)
  
  -- First, decide if the same thing was clicked
  if (MassRefine.nWaitingSlot~=0)and(MassRefine.nWaitingSlot~=nClickSlot)and(MassRefine.nWaitingLoc~=nClickLoc) then
    -- Different slot, do single click behavior on old slot
    local nSlot=MassRefine.nWaitingSlot
    local nLoc=MassRefine.nWaitingLoc
    MassRefine.nWaitingSlot=0 --This way an error in external code doesn't cause unintended behaviour
    MassRefine.nWaitingLoc=0
    MassRefine.OldConfirmThenRefine(nSlot,nLoc)
  end

  -- Get info on the item in question
  local vBag
  if nClickLoc==EA_Window_Backpack.TYPE_INVENTORY then
    vBag=DataUtils.GetItems()
  elseif nClickLoc==EA_Window_Backpack.TYPE_CRAFTING then
    vBag=DataUtils.GetCraftingItems()
  elseif nClickLoc==EA_Window_Backpack.TYPE_CURRENCY then
    vBag=DataUtils.GetCurrencyItems()
  else
    MassRefine.OldConfirmThenRefine(nClickSlot,nClickLoc)
    return
  end
  
  local itemData=vBag[nClickSlot]
  if not itemData then
    return
  end

  -- Is this the first click or the second?
  if MassRefine.nWaitingSlot~=nClickSlot then
    -- First click; setup to wait for the second
    MassRefine.nWaitingSlot=nClickSlot
    MassRefine.nWaitingLoc=nClickLoc
    MassRefine.nWaitingSince=MassRefine.nTime
  else
    -- Second click; open mass refine dialog
    MassRefine.nWaitingSlot=0
    MassRefine.nWaitingLoc=0
    MassRefine.vDialogItem=itemData

    LabelSetText(MassRefine.sWindowName.."ItemText",towstring(MassRefine.GetItemCount(itemData.uniqueID))..L"x"..itemData.name)
    TextEditBoxSetText(MassRefine.sWindowName.."TextInput",L"1")
  
    --Setting the anchor position to be beneath the mouse cursor
    MassRefine.HandleAnchor()
  
    WindowSetShowing(MassRefine.sWindowName,true)
    WindowAssignFocus(MassRefine.sWindowName.."TextInput",true)
    TextEditBoxSelectAll(MassRefine.sWindowName.."TextInput")
  end
end

--Place the window where the mouse cursor currently is
function MassRefine.HandleAnchor()
    local x=SystemData.MousePosition.x/InterfaceCore.GetScale()-100
    local y=SystemData.MousePosition.y/InterfaceCore.GetScale()-15
    WindowClearAnchors(MassRefine.sWindowName)
    WindowAddAnchor(MassRefine.sWindowName,"topleft","Root","topleft",x,y)  
end

-- On Escape key pressed
function MassRefine.OnKeyEscape()
    MassRefine.OnClose()
end

--On Enter key pressed
function MassRefine.OnKeyEnter()
    MassRefine.OkayButton()
end

--On Okay Button LButtonUp pressed
function MassRefine.OkayButton()
    local sInputText=TextEditBoxGetText(MassRefine.sWindowName.."TextInput")
    local nCount=0
    if sInputText then
      nCount=tonumber(WStringToString(sInputText))
    end
    
    if nCount>0 then
      MassRefine.RefineItem(MassRefine.vDialogItem.uniqueID,nCount)
    end

    MassRefine.OnClose()
end

--On Cancel Button LButtonUp pressed
function MassRefine.CancelButton()
    MassRefine.OnClose()
end

function MassRefine.OnClose()
    MassRefine.vDialogItem=nil
    WindowSetShowing(MassRefine.sWindowName,false)
end

--The API requires slot numbers.  We work with uniqueIDs
function MassRefine.GetSlotByItemId(uniqueID,aExclude)
  local vBagItems=DataUtils.GetItems()
  local iSmall=10000
  local iSlot=0
  local nLoc=0
  local vItem
  for k,v in pairs(vBagItems) do
    if (v.uniqueID==uniqueID) and (v.stackCount<iSmall) then
      if (not aExclude) or (not aExclude[EA_Window_Backpack.TYPE_INVENTORY][k]) then
        iSlot=k
        iSmall=v.stackCount
        nLoc=EA_Window_Backpack.TYPE_INVENTORY
        vItem=v
      end
    end
  end
  vBagItems=DataUtils.GetCraftingItems()
  for k,v in pairs(vBagItems) do
    if (v.uniqueID==uniqueID) and (v.stackCount<iSmall) then
      if (not aExclude) or (not aExclude[EA_Window_Backpack.TYPE_CRAFTING][k]) then
        iSlot=k
        iSmall=v.stackCount
        nLoc=EA_Window_Backpack.TYPE_CRAFTING
        vItem=v
      end
    end
  end
  vBagItems=DataUtils.GetCurrencyItems()
  for k,v in pairs(vBagItems) do
    if (v.uniqueID==uniqueID) and (v.stackCount<iSmall) then
      if (not aExclude) or (not aExclude[EA_Window_Backpack.TYPE_CURRENCY][k]) then
        iSlot=k
        iSmall=v.stackCount
        nLoc=EA_Window_Backpack.TYPE_CURRENCY
        vItem=v
      end
    end
  end
  return iSlot,nLoc,vItem
end

function MassRefine.RefineItem(uniqueID,nCount)
  local nSlot,nLoc,vItem
  local aExclude={
    [EA_Window_Backpack.TYPE_INVENTORY]={},
    [EA_Window_Backpack.TYPE_CRAFTING]={},
    [EA_Window_Backpack.TYPE_CURRENCY]={}
  }
  local nLeft=nCount
  while nLeft>0 do
    nSlot,nLoc,vItem=MassRefine.GetSlotByItemId(uniqueID,aExclude)
    if nSlot~=0 then
      aExclude[nLoc][nSlot]=true
      local nGlobalLoc=EA_Window_Backpack.GetCursorForBackpack(nLoc)
      if vItem and vItem.isRefinable then
        if vItem.stackCount>nLeft then
          for i=1,nLeft do
            SendUseItem(nGlobalLoc,nSlot,0,0,0)
          end
          nLeft=0
        else
          nLeft=nLeft-vItem.stackCount
          for i=1,vItem.stackCount do
            SendUseItem(nGlobalLoc,nSlot,0,0,0)
          end
        end
      end
    else
      return
    end
  end
end

--This function determines how many of uniqueID are available
function MassRefine.GetItemCount(uniqueID)
  local vBagItems=DataUtils.GetItems();
  local iCount=0
  --Count items in our bag
  for k,v in pairs(vBagItems) do
    if v.uniqueID==uniqueID then
      iCount=iCount+v.stackCount
    end
  end
  vBagItems=DataUtils.GetCraftingItems()
  for k,v in pairs(vBagItems) do
    if v.uniqueID==uniqueID then
      iCount=iCount+v.stackCount
    end
  end
  vBagItems=DataUtils.GetCurrencyItems()
  for k,v in pairs(vBagItems) do
    if v.uniqueID==uniqueID then
      iCount=iCount+v.stackCount
    end
  end
  return iCount
end

