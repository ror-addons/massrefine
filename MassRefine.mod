<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >
  <UiMod name="Mass Refine" version="1.03" date="18/08/2009" >
    <VersionSettings gameVersion="1.3.6" windowsVersion="1.00" savedVariablesVersion="1.00" />

    <Author name="Irinia of Volkmar" />
    <Description text="Double Ctrl-Right-Click to refine any number of items at one time" />

    <Dependencies>
      <Dependency name="EA_BackpackWindow" />
      <Dependency name="EASystem_Utils" />
    </Dependencies>

    <Files>
      <File name="MassRefine.lua" />
      <File name="MassRefine.xml" />
    </Files>

    <OnInitialize>
      <CallFunction name="MassRefine.Initialize" />
    </OnInitialize>

		<OnUpdate>
		  <CallFunction name="MassRefine.OnUpdate" />
		</OnUpdate>

    <WARInfo>
      <Categories>
        <Category name="ITEMS_INVENTORY" />
        <Category name="CRAFTING" />
      </Categories>
    </WARInfo>
  </UiMod>

</ModuleFile>
